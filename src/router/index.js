import Vue from 'vue'
import VueRouter from 'vue-router'
import Landing from '@/views/Landing.vue'
import Counters from '@/views/Counters.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'landing',
    component: Landing
  },
  {
    path: '/counters',
    name: 'counters',
    component: Counters
  }
]

const router = new VueRouter({
  routes
})

export default router
