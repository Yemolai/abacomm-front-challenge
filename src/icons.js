import Unicon from 'vue-unicons'
import { uniSearch, uniAngleDown, uniBars } from 'vue-unicons/src/icons'

Unicon.add([
  uniSearch,
  uniAngleDown,
  uniBars
])

export default Unicon
