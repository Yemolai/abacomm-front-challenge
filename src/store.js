import Vue from 'vue'

export const state = Vue.observable({
  categorias: 0,
  empresas: 10,
  onde: 0
})

export const mutations = {
  setCategorias (value) {
    state.categorias = value
  },
  setEmpresas (value) {
    state.empresas = value
  },
  setOnde (value) {
    state.onde = value
  }
}

export default (Vue) => {
  Vue.prototype.$store = {
    state,
    mutations
  }
}
