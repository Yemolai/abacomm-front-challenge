import Vue from 'vue'

import App from './App.vue'
import router from './router'
import Unicon from './icons'
import store from './store'

Vue.use(store)

Vue.use(Unicon)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
