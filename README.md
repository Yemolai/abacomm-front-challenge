# Abacomm Frontend Developer Technical Test

This repository has an implementation in Vue of a designed webpage route, just one route is being implemented here. Just for testing purposes. If you find this code interesting or useful, feel free to learn and/or contribute with code or constructive feedback. And I will be happy to talk about it.

## Dependencies

This project use `yarn` to manage its dependencies and run its scripts, the code pieces presented below are intended to be used with `yarn` being a globally available node package. If you don't have `yarn` installed please install it with `npm i -g yarn` before working in this project.

It isn't encouraged to use `npm` to install this projects dependencies as the `lock` file being from `yarn` can result in `npm` using different package versions and possibly having issues, risking even being unable to start it properly.

### Install dependencies

```
yarn install
```

### Package dependencies

- [core-js](https://ghub.io/core-js): Standard library
- [vue](https://ghub.io/vue): Reactive, component-oriented view layer for modern web interfaces.
- [vue-router](https://ghub.io/vue-router): Official router for Vue.js 2
- [vue-unicons](https://ghub.io/vue-unicons): 1000+ Pixel-perfect svg icons for your next project as Vue components

### Development dependencies

- [@babel/core](https://ghub.io/@babel/core): Babel compiler core.
- [@storybook/addon-actions](https://ghub.io/@storybook/addon-actions): Action Logger addon for storybook
- [@storybook/addon-links](https://ghub.io/@storybook/addon-links): Story Links addon for storybook
- [@storybook/addons](https://ghub.io/@storybook/addons): Storybook addons store
- [@storybook/preset-scss](https://ghub.io/@storybook/preset-scss): SCSS preset for Storybook
- [@storybook/vue](https://ghub.io/@storybook/vue): Storybook for Vue: Develop Vue Component in isolation with Hot Reloading.
- [@vue/cli-plugin-babel](https://ghub.io/@vue/cli-plugin-babel): babel plugin for vue-cli
- [@vue/cli-plugin-e2e-cypress](https://ghub.io/@vue/cli-plugin-e2e-cypress): e2e-cypress plugin for vue-cli
- [@vue/cli-plugin-eslint](https://ghub.io/@vue/cli-plugin-eslint): eslint plugin for vue-cli
- [@vue/cli-plugin-unit-mocha](https://ghub.io/@vue/cli-plugin-unit-mocha): mocha unit testing plugin for vue-cli
- [@vue/cli-service](https://ghub.io/@vue/cli-service): local service for vue-cli projects
- [@vue/eslint-config-standard](https://ghub.io/@vue/eslint-config-standard): eslint-config-standard for vue-cli
- [@vue/test-utils](https://ghub.io/@vue/test-utils): Utilities for testing Vue components.
- [babel-eslint](https://ghub.io/babel-eslint): Custom parser for ESLint
- [babel-loader](https://ghub.io/babel-loader): babel module loader for webpack
- [babel-preset-vue](https://ghub.io/babel-preset-vue): Babel preset for all Vue plugins.
- [chai](https://ghub.io/chai): BDD/TDD assertion library for node.js and the browser. Test framework agnostic.
- [css-loader](https://ghub.io/css-loader): css loader module for webpack
- [eslint](https://ghub.io/eslint): An AST-based pattern checker for JavaScript.
- [eslint-plugin-vue](https://ghub.io/eslint-plugin-vue): Official ESLint plugin for Vue.js
- [node-sass](https://ghub.io/node-sass): Wrapper around libsass
- [sass-loader](https://ghub.io/sass-loader): Sass loader for webpack
- [style-loader](https://ghub.io/style-loader): style loader module for webpack
- [vue-template-compiler](https://ghub.io/vue-template-compiler): template compiler for Vue 2.0

## How to run

The project runs on development mode serving the application at [localhost:8080](http://localhost:8080/) when running with `yarn dev`, to serve the storybook with application components use `yarn storybook` and it will serve on [localhost:6006](http://localhost:6006/).

### Serve for development with hot-reloading
```
yarn dev
```

#### Component Storybook with hot-reloading
```
yarn storybook
```

### Compiles and minifies for production

#### The application
```
yarn build
```

#### The component Storybook
```
yarn build-storybook
```

### Lints and fixes files
```
yarn lint
```

## Author

[Romulo G Rodrigues (Yemolai)](https://romulogabriel.com)

Web Fullstack Developer - Computer Engineer

## Contributions

As this code is a test, I'm happy to talk about it and receive feedback, but I can't receive contributions. Feel free to fork it though.
