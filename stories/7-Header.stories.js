import Header from '../src/components/Header/Header.vue'

export default {
  title: 'Page Header'
}

export const example = () => ({
  name: 'Example',
  components: { Header },
  template: `
    <Header/>
  `
})
