const styles = {
  main: {
    padding: 15,
    lineHeight: 1.4,
    fontFamily: '"Helvetica Neue", Helvetica, "Segoe UI", Arial, freesans, sans-serif',
    backgroundColor: '#ffffff'
  },

  code: {
    fontSize: 15,
    fontWeight: 600,
    padding: '2px 5px',
    border: '1px solid #eae9e9',
    borderRadius: 4,
    backgroundColor: '#f3f2f2',
    color: '#3a3a3a'
  },

  note: {
    opacity: 0.5
  }
}

export default {
  name: 'welcome',
  data () {
    return {
      styles
    }
  },

  template: `
    <div :style="styles.main">
      <h1>Abacomm Frontend test STORYBOOK</h1>
      <p>
        This Storybook allows to see use cases, test and understand better the application components.
      </p>
      <p>
        I've put components stories inside the <code :style="code">src/stories</code> directory.
        <br/>
        A story is a single state of one or more UI components. I've written some stories to show
        off the test application components, their usage and capabilities.
      </p>
      <p :style="styles.note">
        Those components are built based on a given design, I wasn't the one who designed them.
      </p>
    </div>
  `,
}
