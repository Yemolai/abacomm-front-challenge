import GridDotBackground from '../src/components/GridDotBackground/GridDotBackground.vue'

export default {
  title: 'GridDotBackground'
}

export const basic = () => ({
  name: 'BasicUsage',
  components: { GridDotBackground },
  template: `
    <GridDotBackground style="height: 200px; width: 200px;"/>
  `
})
