import TopbarLink from '../src/components/Topbar/TopbarLink.vue'
import Topbar from '../src/components/Topbar/Topbar.vue'
import Logo from '../src/assets/png/ben-4-you-logo.png'

export default {
  title: 'Top Bar'
}

export const topBarLink = () => ({
  name: 'TopBarLink',
  components: { TopbarLink },
  template: `
    <div style="display: flex; flex-direction: row;">
      <TopbarLink>Benefícios</TopbarLink>
      <TopbarLink>Ajuda</TopbarLink>
      <TopbarLink active>Contato</TopbarLink>
      <TopbarLink>Entrar</TopbarLink>
    </div>
  `
})

export const TopBar = () => ({
  name: 'TopBar',
  components: {
    TopbarLink,
    Topbar
  },
  data () {
    return { Logo }
  },
  template: `
    <Topbar>
      <template v-slot:logo>
        <img :src="Logo">
      </template>
      <TopbarLink>Benefícios</TopbarLink>
      <TopbarLink>Ajuda</TopbarLink>
      <TopbarLink active>Contato</TopbarLink>
      <TopbarLink>Entrar</TopbarLink>
    </Topbar>
  `
})
