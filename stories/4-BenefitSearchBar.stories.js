import BenefitDropDown from '../src/components/BenefitSearchBar/BenefitDropDown.vue'
import BenefitSearchBar from '../src/components/BenefitSearchBar/BenefitSearchBar.vue'

export default {
  title: 'Benefit Search Bar'
}

export const basicDropDown = () => ({
  name: 'BasicDropDown',
  components: { BenefitDropDown },
  template: `
    <div style="display: flex; flex-direction: row; margin-top: 1em;">
      <BenefitDropDown>Basic</BenefitDropDown>
      <BenefitDropDown active :counter="3">Basic Active</BenefitDropDown>
      <BenefitDropDown label="Labeled" :counter="15" />
      <BenefitDropDown label="Labeled Active" active />
    </div>
  `
})

export const basicSearchBar = () => ({
  name: 'BasicSearchBar',
  components: {
    BenefitSearchBar,
    BenefitDropDown
  },
  template: `
    <div style="display: flex;">
      <BenefitSearchBar>
        <BenefitDropDown active>Categoria</BenefitDropDown>
        <BenefitDropDown>Empresas</BenefitDropDown>
        <BenefitDropDown>Onde</BenefitDropDown>
      </BenefitSearchBar>
    </div>
  `
})
