import SlopedImage from '../src/components/SlopedImage/SlopedImage.vue'

export default {
  title: 'SlopedImage'
}

export const basic = () => ({
  name: 'BasicUsage',
  components: { SlopedImage },
  template: `
    <SlopedImage
      src="https://source.unsplash.com/3840x1080/?city,buildings,office"
    />
  `
})

export const watermark = () => ({
  name: 'Watermark',
  components: { SlopedImage },
  template: `
    <SlopedImage
      watermark
      src="https://source.unsplash.com/3840x1080/?city,buildings,office"
    />
  `
})
