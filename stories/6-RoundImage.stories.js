import RoundImage from '../src/components/RoundImage/RoundImage.vue'
import Bubbles from '../src/components/RoundImage/Bubbles'

export default {
  title: 'Round Image'
}

export const BasicUsage = () => ({
  name: 'BasicUsage',
  components: { RoundImage },
  data () {
    return {
      unsplashLink: 'https://source.unsplash.com/600x600/?warm,vacations,couple'
    }
  },
  template: `
    <div style="height: 320px; width: 320px; background-color: #AAA; padding: 10vh 10vw;">
      <RoundImage :src="unsplashLink" />
    </div>
  `
})

export const BubblesExample = () => ({
  name: 'BubblesExample',
  components: { Bubbles },
  template: `
    <div style="height: 320px; width: 320px; background-color: #AAA; padding: 10vh 10vw;">
      <Bubbles style="z-index: 1;" />
    </div>
  `
})
