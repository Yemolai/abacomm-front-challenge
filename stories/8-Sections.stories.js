import BenefitSection from '../src/components/Section/BenefitSection'
import HowItWorksSection from '../src/components/Section/HowItWorksSection'

export default {
  title: 'Sections'
}

export const Benefit = () => ({
  name: 'Benefit',
  components: { BenefitSection },
  template: `
    <BenefitSection/>
  `
})

export const HowItWorks = () => ({
  name: 'HowItWorks',
  components: { HowItWorksSection },
  template: `
    <HowItWorksSection/>
  `
})
