import Vue from 'vue'
import { configure } from '@storybook/vue';
import icons from '../src/icons';
import store from '../src/store'

Vue.use(icons)
Vue.use(store)

// automatically import all files ending in *.stories.js
configure(require.context('../stories', true, /\.stories\.js$/), module);
